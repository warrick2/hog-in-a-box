FROM mailhog/mailhog

USER root
RUN apk add --no-cache gettext
USER mailhog

COPY gmail-smtp-template.json gmail-smtp-template.json
COPY entrypoint.sh entrypoint.sh

ENV MH_STORAGE=maildir
ENV MH_MAILDIR_PATH=data

RUN mkdir -p data

ENTRYPOINT [ "./entrypoint.sh" ]
CMD [ "MailHog" ]