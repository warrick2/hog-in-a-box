#!/bin/sh

echo "Generating Gmail SMTP config"
envsubst < gmail-smtp-template.json > gmail-smtp.json

export MH_OUTGOING_SMTP=gmail-smtp.json

exec "$@"