# MailHog for Development Email testing

This project is essentially a docker compose configuration for running MailHog, with your Gmail account setup for forwarding emails to real inboxes.

By default, it will run untill you stop it, and email data will be persisted in a docker volume.

Getting setup:
- Copy `template.env` to `.env` and fill it out with your Gmail credentials
- Run `docker compose up -d` to start MailHog in the background
- Go to `http://localhost:8025` to access the MailHog web UI

To stop everything, run `docker compose down`.

The smpt port for sending emails to MailHog is exposed on 1025.